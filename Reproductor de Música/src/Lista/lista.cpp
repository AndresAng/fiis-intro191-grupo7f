/*
 * lista.cpp
 *
 *  Created on: 2 jun. 2019
 *      Author: ANDRES
 */
#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include "../Utilitarios/Utilitarios.hpp"
#include "../Musicas/Musicas.hpp"
using namespace std;

struct Musica can[100]={
		{"A Head Full of Dreams - Coldplay", "Pop", "4:59"}, {"Cuando pienses en volver - Pedro Suarez Vertiz", "Pop", "4:15"}, {"Sweet Child O' Mine - Guns N' Roses", "Rock", "4:59"}, {"Innuendo - Queen", "Rock", "6:33"}, {"Time in a Bottle - Jim Croce", "Rock", "2:25"}, {"Another One Bytes the Dust - Queen", "Rock", "3:36"}, {"Wonderwall - Oasis", "Rock", "4:37"}, {"Counting Stars - One Republic", "Pop", "4:18"}, {"Bohemian Rhapsody - Queen", "Rock", "6:06"}, {"Persiana Americana - Soda Stereo", "Rock", "5:52"},
		{"Wish You Were Here", "Rock", "5:34"}, {"Yesterday - The Beatles", "Rock", "2:02"}, {"Nandemonaiya - Radwimps", "Rock", "5:45"}, {"Perfect - Ed Sheeran","Pop","4:21"}, {"Stairway to Heaven - Led Zeppelin", "Rock", "8:02"}, {"Sweet Dreams - Eurythmics", "Pop", "3:34"}, {"Profugos - Soda Stereo", "Rock", "5:52"}, {"Don't Look Back in Anger - Oasis", "Rock", "4:47"}, {"The Scientist", "Pop", "5:12"}, {"I Ran - A Flock of Seagulls", "Pop", "3:58"},
		{"Prayer in C - Lilly Wood & The Prick and Robin Schulz", "Electronica", "3:14"}, {"Another Brick in the Wall - Pink Floyd", "Rock", "6:01"}, {"Cliffs of Dover - Eric Johnson", "Rock", "4:13"}, {"Reptilia - The Strokes", "Rock", "3:35"}, {"Nada Personal - Soda Stereo", "Rock", "4:53"},
		{"Howling - FLOW X GRANRODEO","Rock","4:21"},{"Eco - Crusher-P","Electronica","4:04"},{"Prince Ali - Annapantsu","Musica infantil","2:51"},{"Magia - Kalafina","Pop","5:09"},{"Connect - ClariS","Anison","4:26"},{"Peace sing - Kenshi Yonezu","Pop","4:00"},{"X4B The Guardian - Koji Wada","Anison","4:26"},{"Reason Living - SCREEN mode","Anison","4:42"},
		{"Deadly Drive - GRANRODEO","Anison","4:35"},{"Luck Life - Bakura","Anison","5:38"},{"Stronger Than You - Rebecca Sugar","Musica infantil","2:52"},{"Disbelief - BB� Mafia","Rap","3:32"},{"Du Hast - Rammstein","Rock","3:55"},{"El Gran Varon - Willie Colon","Salsa","6:53"},{"Chop Suey - System Of A Down","Rock","3:26"},{"Baby Shark - Pinkfong","Musica infantil","2:16"},
		{"Laura no esta - Nek","Pop","3:50"},{"Brave Heart - Ayumi Miyazaki","Anison","4:12"},{"It's Payday - Simon Viklund","Rock","4:36"},{"Pimped Out Getaway - Simon Viklund","Rock","4:17"},{"Megalovania - Toby Fox","Pop","2:37"},{"Rap God - Eminem","Rap","6:09"}, {"the WORLD - Nightmare","Rock","4:02"},{"THERE IS A REASON - Konomi Suzuki","Anison","4:52"},{"ODD FUTURE - UVERworld","Rap","3:50"},
		{"Believer - Imagine Dragons", "Rock", "3:36"}, {"Sandstorn - Darude", "Electronica", "3:52"}, {"Paint It Black - The Rolling Stones", "Rock", "3:46"}, {"Played a live - Safri Duo", "Electronica", "3:11"}, {"I was made for loving you - Kiss", "Rock", "3:58"}, {"La vida loca - Ricky Martin", "Pop", "3:42"}, {"A Dios Le Pido - Juanes", "Pop", "3:26"}, {"What Is Love - Haddaway", "Pop", "4:00"},
		{"Magic In The Air - Magic System", "Pop", "3:54"}, {"Subeme la radio - Enrique Iglesias", "Pop", "3:52"}, {"Rewrite - Asian Kung Fu Generations", "Rock", "3:47"}, {"Shamandalie - Sonata Artica", "Rock", "4:01"}, {"Tik Tok - Kesha", "Pop", "3:34"}, {"Glad You Came - The Wanted", "Pop", "3:22"}, {"We Will Rock You - Queen", "Rock", "2:14"}, {"Globos Del Cielo - Pedro Suarez Vertiz", "Rock", "3:53"}, {"Never be alone - TheFatRat", "Electronica", "4:20"}, {"Se me olvido otra vez - Mana", "Rock", "3:45"},
		{"LAS TORRES - Los Nosequien y Los Nosecuantos", "Rock", "3:10"}, {"Alone - Marshmello", "Electronica", "3:19"}, {"Move Like Jagger - Maroon 5", "Electronica", "4:38"}, {"Shape of Your - Ed Sheeran", "Pop", "4:23"}, {"Amante Bandido - Miguel Bose", "Pop", "4:20"}, {"Havana - Camila Cabello", "Pop", "3:38"}, {"Desde Cuando - Alejandro Sanz", "Pop", "4:02"},
		{"Yellow Submarine - The Beatles", "Rock", "2:45"}, {"November Rain - Guns N' Roses", "Rock", "9:17"}, {"Bailando - Enrique Iglesias", "Pop", "4:47"}, {"Genie in a Bottle - Christina Aguilera", "Pop", "3:37"}, {"In The End - Linkin Park", "Rock", "3:38"}, {"Us and them - Pink Floyd", "Rock", "6:01"}, {"Dream On - Aerosmith", "Rock", "4:24"}, {"Thriller - Michael Jackson", "Pop", "13:43"}, {"Like a Prayer - Madonna", "Pop", "5:38"}, {"Baby One More Time - Britney Spears", "Pop", "3:57"},
		{"Everybody - Backstreet Boys", "Pop", "6:01"}, {"Hotel California - The Eagles", "Rock", "6:31"}, {"Billie Jean - Michael Jackson", "Pop", "4:56"}, {"Smells like Teen Spirit - Nirvana", "Rock", "4:39"}, {"Uptown Funk - Bruno Mars", "Pop", "4:31"}, {"Live Forever - Oasis", "Rock", "4:38"}, {"Back in Black - AC/DC", "Rock", "4:15"}, {"Rock and Roll at Nite - Kiss", "Rock", "2:49"}, {"Party in the USA - Miley Cyrus", "Pop", "3:22"}, {"Single Ladies - Beyonce", "Pop", "3:19"},
		{"Immigrant Song - Led Zeppelin", "Rock", "2:28"}, {"Run To The Hills - Iron Maiden", "Rock", "6:55"}, {"Shake it Off - Taylor Swift", "Pop", "4:02"}, {"Rolling in the Deep - Adele", "Pop", "3:54"}, {"Don�t Stop Believing - Journey", "Pop", "4:16"}

	};

struct Musica Listas[5][100]={{
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}},
		{	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
			{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}},
			{	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
				{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}},
				{	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
					{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}},
					{	{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"},
						{"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}, {"0","0","0"}}
};

void Imprimir(Musica m){
	cout << m.Nombre << " " << m.Genero << " " << m.Duracion;
}

void PestLista(){
	int x = -1;int cte2 = 0; int w = 0;
		while (x != 0){
			system("cls");
			cout<<"\n\tLISTAS PERSONALIZADAS\n\t";
			for(int i=1;i<25;i++){
				cout<<char(196);
			}cout<<endl<<endl;
			cout << "1.- Pop" << endl;
			cout << "2.- Rock" << endl;
			cout << "3.- Electronica" << endl;
			cout << "4.- Musica Infatil" << endl;
			cout << "5.- Rap" << endl;
			cout << "6.- Anison" << endl;
			cout << "7.- Salsa" << endl;
			for (int i = 0; i < 5; i++){
				int cont1 = 0;
				for(int j = 0; j < 100; j++){
				if(Listas[i][j].Nombre != "0" && Listas[i][j].Genero != "0"){
					cont1++;
					}
				}
				if (cont1 != 0){
					cout << i+8 << ".- Lista " << i+1 << endl;
					cte2 = i + 8;
					}
				}
			cout << "99.- Crear una nueva lista" << endl;
			cout << "0.- Atras" << endl;
			cout << "Escoja el n"<<char(163)<<"mero de la opci"<<char(162)<<"n: ";
			cin >> x;
			if(x == 1){
			int m = 0;
			while (m != 2){
				int cte = 0;
				m = 0;
				system("cls");
				for(int n = 0; n < 100; n++){
					if (can[n].Genero == "Pop"){
					int h = recuadro(can[n]);
					system("cls");
					if (h == 1){
						cte = 1;
						break;
					}
					if(n >= 1 && h == 2){
						if (n == 0){n = -1;} else {n -= 2;}
					}
					}
				}
				if (cte != 1){while (m != 1 && m != 2){ system("cls");
				cout << "�Volver a reproducir?" << endl;
				cout << "1.- Si" << endl << "2.- No" << endl;
				cout << "Introduzca opcion numerica: ";
				cin >> m;}} else {break;}
			}
			}
			if(x == 2){
				int m = 0;
				while (m != 2){
					int cte = 0;
					m = 0;
					system("cls");
					for(int n = 0; n < 100; n++){
						if (can[n].Genero == "Rock"){
							int h = recuadro(can[n]);
							system("cls");
							if (h == 1){
								cte = 1;
								break;
							}
							if(n >= 1 && h == 2){
								if (n == 0){n = -1;} else {n -= 2;}
							}
						}
					}if (cte != 1){while (m != 1 && m != 2){system("cls");
					cout << "�Volver a reproducir?" << endl;
					cout << "1.- Si" << endl << "2.- No" << endl;
					cout << "Introduzca opcion numerica: ";
					cin >> m;}} else {break;}
				}
			}
			if(x == 3){
				int m = 0;
				while (m != 2){
					int cte = 0;
					m = 0;
					system("cls");
					for(int n = 0; n < 100; n++){
						if (can[n].Genero == "Electronica"){
							int h = recuadro(can[n]);
							system("cls");
							if (h == 1){
								cte = 1;
								break;
							}
							if(n >= 1 && h == 2){
								if (n == 0){n = -1;} else {n -= 2;}
							}
						}
					}
					if (cte != 1){while (m != 1 && m != 2){system("cls");
						cout << "�Volver a reproducir?" << endl;
						cout << "1.- Si" << endl << "2.- No" << endl;
						cout << "Introduzca opcion numerica: ";
						cin >> m;}} else {break;}
				}
			}
			if(x == 4){
				int m = 0;
				while (m != 2){
					int cte = 0;
					m = 0;
					system("cls");
					for(int n = 0; n < 100; n++){
						if (can[n].Genero == "Musica infantil"){
						int h = recuadro(can[n]);
						system("cls");
						if (h == 1){
							cte = 1;
							break;
						}
						if(n >= 1 && h == 2){
							if (n == 0){n = -1;} else {n -= 2;}
						}
						}
					}
					if (cte != 1){while (m != 1 && m != 2){system("cls");
					cout << "�Volver a reproducir?" << endl;
					cout << "1.- Si" << endl << "2.- No" << endl;
					cout << "Introduzca opcion numerica: ";
					cin >> m;}} else {break;}
				}
			}
			if(x == 5){
				int m = 0;
				while (m != 2){
					int cte = 0;
					m = 0;
					system("cls");
					for(int n = 0; n < 100; n++){
						if (can[n].Genero == "Rap"){
						int h = recuadro(can[n]);
						system("cls");
						if (h == 1){
							cte = 1;
							break;
						}
						if(n >= 1 && h == 2){
							if (n == 0){n = -1;} else {n -= 2;}
						}
						}
					}
					if (cte != 1){while (m != 1 && m != 2){system("cls");
					cout << "�Volver a reproducir?" << endl;
					cout << "1.- Si" << endl << "2.- No" << endl;
					cout << "Introduzca opcion numerica: ";
					cin >> m;}} else {break;}
					}
				}
			if(x == 6){
				int m = 0;
				while (m != 2){
					int cte = 0;
					m = 0;
					system("cls");
					for(int n = 0; n < 100; n++){
						if (can[n].Genero == "Anison"){
						int h = recuadro(can[n]);
						system("cls");
						if (h == 1){
							cte = 1;
							break;
						}
						if(n >= 1 && h == 2){
							if (n == 0){n = -1;} else {n -= 2;}
						}
						}
					}
					if (cte != 1){ while (m != 1 && m != 2){system("cls");
					cout << "�Volver a reproducir?" << endl;
					cout << "1.- Si" << endl << "2.- No" << endl;
					cout << "Introduzca opcion numerica: ";
					cin >> m;}} else {break;}
				}
				}
			if(x == 7){
				int m = 0;
				while (m != 2){
					m = 0;
					int cte = 0;
					system("cls");
					for(int n = 0; n < 100; n++){
						if (can[n].Genero == "Salsa"){
						int h = recuadro(can[n]);
						system("cls");
						if (h == 1){
							cte = 1;
							break;
						}
						if(n >= 1 && h == 2){
							if (n == 0){n = -1;} else {n -= 2;}
						}
						}
					}
					if (cte != 1){while (m != 1 && m != 2){system("cls");
					cout << "�Volver a reproducir?" << endl;
					cout << "1.- Si" << endl << "2.- No" << endl;
					cout << "Introduzca opcion numerica: ";
					cin >> m;}} else {break;}
				}
				}
			if(x >= 8 && x < 13 && cte2 >= x){int m = 0;
				while (m != 2){
					m = 0;
					int cte = 0;
					system("cls");
						for(int j = 0; j < 100; j++){
						if (Listas[x - 8][j].Nombre != "0" && Listas[x - 8][j].Genero != "0"){
							int h = recuadro(Listas[x - 8][j]);
							system("cls");
							if (h == 1){
								cte = 1;
								break;
							}
							if (j >= 1 && h == 2){
								if (j == 0){j = -1;} else {j -= 2;}
							}
						}
					}
					if (cte != 1){while (m != 1 && m != 2){system("cls");
						cout << "�Volver a reproducir?" << endl;
						cout << "1.- Si" << endl << "2.- No" << endl;
						cout << "Introduzca opcion numerica: ";
						cin >> m;}} else {break;}
				}
			}
			if(x == 99 && cte2 != 13){int n = -1; int m = 0;
				while (n != 0){
					system("cls");
					for (int i = 0; i < 100; i++){
						cout << i+1 << ".- " << can[i].Nombre << " / " << can[i].Genero << " / " << can[i].Duracion << " " << endl;
					}
					cout << "Escoja la musica o 0 para terminar, puedes escoger la misma cancion las veces que quieras. Tienes un limite de 100 canciones y 5 listas: "; cin >> n;
					if (n != 0){
						Listas[w][m].Nombre = can[n-1].Nombre;
						Listas[w][m].Genero = can[n-1].Genero;
						Listas[w][m].Duracion = can[n-1].Duracion;
						m++;
						} else if(n == 0) {w++;}
					}
				}
	}
}


