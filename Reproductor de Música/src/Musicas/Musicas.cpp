/*
 * Musicas.cpp
 *
 *  Created on: 18 jun. 2019
 *      Author: JULIO
 */

#include <iostream>
#include <stdlib.h>
#include <windows.h>
#include "../Utilitarios/Utilitarios.hpp"
#include "../Musicas/Musicas.hpp"
using namespace std;

struct Musica canciones[100]={
		{"A Head Full of Dreams - Coldplay", "Pop", "4:59"}, {"Cuando pienses en volver - Pedro Suarez Vertiz", "Pop", "4:15"}, {"Sweet Child O' Mine - Guns N' Roses", "Rock", "4:59"}, {"Innuendo - Queen", "Rock", "6:33"}, {"Time in a Bottle - Jim Croce", "Rock", "2:25"}, {"Another One Bytes the Dust - Queen", "Rock", "3:36"}, {"Wonderwall - Oasis", "Rock", "4:37"}, {"Counting Stars - One Republic", "Pop", "4:18"}, {"Bohemian Rhapsody - Queen", "Rock", "6:06"}, {"Persiana Americana - Soda Stereo", "Rock", "5:52"},
		{"Wish You Were Here", "Rock", "5:34"}, {"Yesterday - The Beatles", "Rock", "2:02"}, {"Nandemonaiya - Radwimps", "Rock", "5:45"}, {"Perfect - Ed Sheeran","Pop","4:21"}, {"Stairway to Heaven - Led Zeppelin", "Rock", "8:02"}, {"Sweet Dreams - Eurythmics", "Pop", "3:34"}, {"Profugos - Soda Stereo", "Rock", "5:52"}, {"Don't Look Back in Anger - Oasis", "Rock", "4:47"}, {"The Scientist", "Pop", "5:12"}, {"I Ran - A Flock of Seagulls", "Pop", "3:58"},
		{"Prayer in C - Lilly Wood & The Prick and Robin Schulz", "Electronica", "3:14"}, {"Another Brick in the Wall - Pink Floyd", "Rock", "6:01"}, {"Cliffs of Dover - Eric Johnson", "Rock", "4:13"}, {"Reptilia - The Strokes", "Rock", "3:35"}, {"Nada Personal - Soda Stereo", "Rock", "4:53"},
		{"Howling - FLOW X GRANRODEO","Rock","4:21"},{"Eco - Crusher-P","Electronica","4:04"},{"Prince Ali - Annapantsu","Musica infantil","2:51"},{"Magia - Kalafina","Pop","5:09"},{"Connect - ClariS","Anison","4:26"},{"Peace sing - Kenshi Yonezu","Pop","4:00"},{"X4B The Guardian - Koji Wada","Anison","4:26"},{"Reason Living - SCREEN mode","Anison","4:42"},
		{"Deadly Drive - GRANRODEO","Anison","4:35"},{"Luck Life - Bakura","Anison","5:38"},{"Stronger Than You - Rebecca Sugar","Musica infantil","2:52"},{"Disbelief - BB� Mafia","Rap","3:32"},{"Du Hast - Rammstein","Rock","3:55"},{"El Gran Varon - Willie Colon","Salsa","6:53"},{"Chop Suey - System Of A Down","Rock","3:26"},{"Baby Shark - Pinkfong","Musica infantil","2:16"},
		{"Laura no esta - Nek","Pop","3:50"},{"Brave Heart - Ayumi Miyazaki","Anison","4:12"},{"It's Payday - Simon Viklund","Rock","4:36"},{"Pimped Out Getaway - Simon Viklund","Rock","4:17"},{"Megalovania - Toby Fox","Pop","2:37"},{"Rap God - Eminem","Rap","6:09"}, {"the WORLD - Nightmare","Rock","4:02"},{"THERE IS A REASON - Konomi Suzuki","Anison","4:52"},{"ODD FUTURE - UVERworld","Rap","3:50"},
		{"Believer - Imagine Dragons", "Rock", "3:36"}, {"Sandstorn - Darude", "Electronica", "3:52"}, {"Paint It Black - The Rolling Stones", "Rock", "3:46"}, {"Played a live - Safri Duo", "Electronica", "3:11"}, {"I was made for loving you - Kiss", "Rock", "3:58"}, {"La vida loca - Ricky Martin", "Pop", "3:42"}, {"A Dios Le Pido - Juanes", "Pop", "3:26"}, {"What Is Love - Haddaway", "Pop", "4:00"},
		{"Magic In The Air - Magic System", "Pop", "3:54"}, {"Subeme la radio - Enrique Iglesias", "Pop", "3:52"}, {"Rewrite - Asian Kung Fu Generations", "Rock", "3:47"}, {"Shamandalie - Sonata Artica", "Rock", "4:01"}, {"Tik Tok - Kesha", "Pop", "3:34"}, {"Glad You Came - The Wanted", "Pop", "3:22"}, {"We Will Rock You - Queen", "Rock", "2:14"}, {"Globos Del Cielo - Pedro Suarez Vertiz", "Rock", "3:53"}, {"Never be alone - TheFatRat", "Electronica", "4:20"}, {"Se me olvido otra vez - Mana", "Rock", "3:45"},
		{"LAS TORRES - Los Nosequien y Los Nosecuantos", "Rock", "3:10"}, {"Alone - Marshmello", "Electronica", "3:19"}, {"Move Like Jagger - Maroon 5", "Electronica", "4:38"}, {"Shape of Your - Ed Sheeran", "Pop", "4:23"}, {"Amante Bandido - Miguel Bose", "Pop", "4:20"}, {"Havana - Camila Cabello", "Pop", "3:38"}, {"Desde Cuando - Alejandro Sanz", "Pop", "4:02"},
		{"Yellow Submarine - The Beatles", "Rock", "2:45"}, {"November Rain - Guns N' Roses", "Rock", "9:17"}, {"Bailando - Enrique Iglesias", "Pop", "4:47"}, {"Genie in a Bottle - Christina Aguilera", "Pop", "3:37"}, {"In The End - Linkin Park", "Rock", "3:38"}, {"Us and them - Pink Floyd", "Rock", "6:01"}, {"Dream On - Aerosmith", "Rock", "4:24"}, {"Thriller - Michael Jackson", "Pop", "13:43"}, {"Like a Prayer - Madonna", "Pop", "5:38"}, {"Baby One More Time - Britney Spears", "Pop", "3:57"},
		{"Everybody - Backstreet Boys", "Pop", "6:01"}, {"Hotel California - The Eagles", "Rock", "6:31"}, {"Billie Jean - Michael Jackson", "Pop", "4:56"}, {"Smells like Teen Spirit - Nirvana", "Rock", "4:39"}, {"Uptown Funk - Bruno Mars", "Pop", "4:31"}, {"Live Forever - Oasis", "Rock", "4:38"}, {"Back in Black - AC/DC", "Rock", "4:15"}, {"Rock and Roll at Nite - Kiss", "Rock", "2:49"}, {"Party in the USA - Miley Cyrus", "Pop", "3:22"}, {"Single Ladies - Beyonce", "Pop", "3:19"},
		{"Immigrant Song - Led Zeppelin", "Rock", "2:28"}, {"Run To The Hills - Iron Maiden", "Rock", "6:55"}, {"Shake it Off - Taylor Swift", "Pop", "4:02"}, {"Rolling in the Deep - Adele", "Pop", "3:54"}, {"Don�t Stop Believing - Journey", "Pop", "4:16"}

	};
void Imprimir1(){
		for(int i = 0; i < 100; i++){
				cout << i + 1 << ".- " << canciones[i].Nombre << " " << canciones[i].Genero << " " << canciones[i].Duracion << endl;
			}
	}

int Imprimirdesde(int x){
	system("cls");
	cout << "Cargando...." << endl;
	Sleep(1000);
	system("cls");
	int cte = 0;
		for(int i = x; i <= 100; i++){
			int h = recuadro(canciones[i-1]);
			system("cls");
			if (h == 1){
				cte = 1;
				break;
			}
			if(i >= 1 && h == 2){
				if (i - 1 == 0){i = -1;} else {i -= 2;}
			}
		}
		return cte;
	}

void Pestreproducirdesde(){
	int a = -1;
			while (a != 0){
				cout<<"\n\tLISTA DE MUSICA\n\t";
				for(int i=1;i<16;i++){
					cout<<char(196);
				}cout<<endl<<endl;
				int n = 0;
				Imprimir1();
					cout << "0.- Salir " << endl;
					cout << "Reproducir desde: ";
					cin >> a;
					if(a == 0){
						break;
					} else if(n!=1 && a > 0 && a < 101){
				while (n != 2){
				system("cls");
				int m = Imprimirdesde(a);
				if (m == 0){
			cout << "�Volver a reproducir?" << endl;
			cout << "1.- Si" << endl;
			cout << "2.- No" << endl;
			cout << "Introduzca opcion numerica: ";
			cin >> n;
			system("cls");}else {break;}
			}
		}
	}
}



