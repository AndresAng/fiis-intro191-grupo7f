/*
 * Utilitarios.cpp
 *
 *  Created on: 21 jun. 2019
 *      Author: AndresAng
 */
#include <iostream>
#include <windows.h>
#include "../Musicas/Musicas.hpp"
using namespace std;

int recuadro(Musica l){
	cout << "Presione ESPACIO si desea Pausar/Continuar la reproduccion" << endl;
	cout << "Presione ESCAPE si desea salir de la reproduccion" << endl;
	cout << "Presione IZQUIERDA si desea retroceder de cancion" << endl;
	cout << "Presione DERECHA si desea pasar a la siguiente cancion" << endl;
	cout<<char(201);
	for(int ancho=1;ancho<=20;ancho++){
		cout<<char(205);
	}cout<<char(187)<<endl;

	for(int largo=1;largo<=7;largo++){
		cout<<char(186);
		for(int lago=1;lago<=20;lago++){
			cout<<char(32);
		}
		cout<<char(186);
		switch(largo){
		case 1:cout<<" "<<l.Nombre;break;
		case 4:cout<<" "<<l.Genero;break;
		case 7:cout<<" "<<l.Duracion;break;
		default: cout<<" ";break;
		}
		cout<<endl;
	}
	cout<<char(200);
		for(int ancho=1;ancho<=20;ancho++){
				cout<<char(205);
		}cout<<char(188)<<endl;
		cout<<" ";
		bool pausado = false;
		int k = 0, v = 0;
		for(int time=1;time<=20;time++){
			while(pausado){
				Sleep(250);
				//En caso se presione la tecla escape
				if(GetAsyncKeyState(VK_ESCAPE)){
					k++;
					v++;
					break;
				}
				//En caso se presione la barra espaciadora
				if(GetAsyncKeyState(VK_SPACE)){
					pausado = !pausado;
				}

				if(GetAsyncKeyState(VK_LEFT)){
					k += 2;
					v += 2;
					break;
				}
				if (GetAsyncKeyState(VK_RIGHT)){
					v += 1;
					break;
				}
			}
			if (v == 1||v == 2){break;}
			Sleep(250);
			cout<<char(219);
			if(GetAsyncKeyState(VK_ESCAPE)){
				k++;
				break;
			}else if(GetAsyncKeyState(VK_SPACE)){
				pausado = !pausado;
			}
			else if(GetAsyncKeyState(VK_LEFT)){
				k += 2;
				break;
			} else if (GetAsyncKeyState(VK_RIGHT)){
				break;
			}
		}
	return k;
}




