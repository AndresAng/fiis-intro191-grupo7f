## Simulador de reproductor de música

# Integrantes:
1. Angeles Leon, Andres Sebastian (Jefe de grupo)
2. Aylas Andia, Julio Daniel
3. Champi Yauli, David Sebastian
4. Machaca Mamani, Luis Alberto

#Link del prototipo:
https://www.youtube.com/watch?v=7JxayG6juqE&t
#Link del original:
https://youtu.be/yoH7DwyUH0c

# Descripción del programa:

El simulador de reproductor de músicas contendrá opciones para una reproducción como está estructurado en la lista principal, una reproducción aleatoria y la posibilidad de formar tres listas de música con un orden distinto al de la lista original y con la cantidad de músicas que se desee.
